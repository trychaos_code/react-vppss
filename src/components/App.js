import React from 'react';
import { Link } from 'react-router';

const App = ({ children }) => (
  <div>
    <header>
      <h1>Ui Training</h1>
      <Link to="/vivek">Vivek</Link>
      <Link to="/suma">Suma</Link>
      <Link to="/prashanthi">Prashanthi</Link>
      <Link to="/srikanth">Srikanth</Link>
      <Link to="/priyanka">Priyanka</Link>
    </header>
    <section>
      {children || 'Welcome to React Class'}
    </section>
  </div>
);

App.propTypes = { children: React.PropTypes.object };

export default App;
