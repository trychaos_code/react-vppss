import React, {Component} from 'react';
import PropTypes from 'prop-types';

export default class MyWizardForName extends Component {
  constructor (props) {
    super(props);
    this.alertMsg = this.alertMsg.bind(this);
  }
  alertMsg(p){
    alert(p);
  }
  render () {
    return (
      <div onClick={()=>{this.alertMsg('Hi Im in wizard component')}}>
        {this.props.name}
      </div>
    )
  }
}

MyWizardForName.propTypes = {
  name: PropTypes.string
};

MyWizardForName.defaultProps = {
  name: 'Akhilesh'
};
